# Desafio Android Eventos #

### Arquitetura  ###
* MVVM pattern
* Clean Architecture
* Modularization

Esta combinação foi escolhida por diversos motivos, destre eles: Independente de Frameworks e banco de dados, testável, código mais organizado, facilidade para manutenção do código, tempo de construção mais rápido.

### Libs ###

* Koin
* ViewModel
* DataBinding
* Navegation
* ktx
* Retrofit
* RxAndroid
* RxKotlin
* Picasso




package com.pabloteigon.events.domain.usecases

import com.pabloteigon.events.domain.repository.EventsRepository
import io.reactivex.Completable
import io.reactivex.Scheduler

interface CheckInUseCase {
    fun execute(body: Map<String, Any>): Completable
}

class CheckInUseCaseImpl(
    private val eventsRepository: EventsRepository,
    private val scheduler: Scheduler
) : CheckInUseCase {

    override fun execute(
        body: Map<String, Any>
    ): Completable {
        return eventsRepository.checkIn(body)
            .subscribeOn(scheduler)
    }
}
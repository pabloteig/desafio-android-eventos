package com.pabloteigon.events.domain.entities

import com.pabloteigon.events.domain.extension.formatToViewDateTimeDefaults
import java.math.BigDecimal
import java.util.*

data class Event(
    val people: List<People>? = null,
    val date: Date? = Date(),
    val description: String? = "",
    val image: String? = "",
    val longitude: String? = "",
    val latitude: String? = "",
    val price: BigDecimal? = BigDecimal.ZERO,
    val title: String? = "",
    val id: String? = "",
    val cupons: List<Cupon>? = null
) {
    fun getEventTextToShare(): String {
        val text = StringBuilder()
        text.append(title).append("\n\n")
        text.append(description).append("\n\n")
        text.append("Data: ").append(date?.formatToViewDateTimeDefaults())
        return text.toString()
    }
}

data class People(
    val id: String?,
    val eventId: String?,
    val name: String?,
    val picture: String?
)

data class Cupon(
    val id: String?,
    val eventId: String?,
    val discount: String?
)
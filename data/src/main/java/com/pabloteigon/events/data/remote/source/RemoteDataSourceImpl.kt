package com.pabloteigon.events.data.remote.source

import com.pabloteigon.events.data.remote.api.ServerApi
import com.pabloteigon.events.data.remote.mapper.EventMapper
import com.pabloteigon.events.domain.entities.Event
import io.reactivex.Completable
import io.reactivex.Single

class RemoteDataSourceImpl(private val serverApi: ServerApi) : RemoteDataSource {

    override fun getListOfEvents(): Single<List<Event>> {
        return serverApi.fetchListOfEvents()
            .map { EventMapper.mapList(it) }
    }

    override fun getEventById(eventId: String): Single<Event> {
        return serverApi.fetchEventById(eventId)
            .map { EventMapper.map(it) }
    }

    override fun checkIn(body: Map<String, Any>): Completable {
        return serverApi.checkIn(body)
    }
}
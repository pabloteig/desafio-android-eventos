package com.pabloteigon.events.data.di

import com.pabloteigon.events.data.repository.EventsRepositoryImpl
import com.pabloteigon.events.domain.repository.EventsRepository
import org.koin.dsl.module

val repositoryModule = module {

    factory<EventsRepository> {
        EventsRepositoryImpl(
            remoteDataSource = get()
        )
    }

}

val dataModules = listOf(remoteDataSourceModule, repositoryModule)
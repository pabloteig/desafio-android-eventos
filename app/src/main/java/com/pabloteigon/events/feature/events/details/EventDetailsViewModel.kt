package com.pabloteigon.events.feature.events.details

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.pabloteigon.events.domain.entities.Event
import com.pabloteigon.events.domain.usecases.CheckInUseCase
import com.pabloteigon.events.domain.usecases.GetEventByIdUseCase
import com.pabloteigon.events.feature.viewmodel.BaseViewModel
import com.pabloteigon.events.feature.viewmodel.StateMachineCompletable
import com.pabloteigon.events.feature.viewmodel.StateMachineSingle
import com.pabloteigon.events.feature.viewmodel.ViewState
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign

class EventDetailsViewModel(
    val getEventByIdUseCase: GetEventByIdUseCase,
    val checkInUseCase: CheckInUseCase,
    val uiScheduler: Scheduler
) : BaseViewModel(), LifecycleObserver {

    var eventId = ""
    val showDescription = MutableLiveData<Boolean>().apply { value = false }
    val event = MutableLiveData<Event>().apply { value = Event() }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onStart() {
        getEventById()
    }

    private fun getEventById() {
        loadingVisibility.value = true
        disposables += getEventByIdUseCase.execute(
            eventId = eventId
        )
            .compose(StateMachineSingle())
            .observeOn(uiScheduler)
            .subscribe(
                {
                    if (it is ViewState.Success) {
                        event.value = it.data

                    } else if (it is ViewState.Failed) {
                        errorState.value = ViewState.Failed(it.throwable)
                    }
                    loadingVisibility.value = false
                },
                {
                    loadingVisibility.value = false
                    errorState.value = ViewState.Failed(it)
                }
            )
    }

    fun checkIn() {
        loadingVisibility.value = true
        disposables += checkInUseCase.execute(getBody())
            .compose(StateMachineCompletable())
            .observeOn(uiScheduler)
            .subscribe(
                {
                    loadingVisibility.value = false

                },
                {
                    loadingVisibility.value = false
                    errorState.value = ViewState.Failed(it)
                }
            )
    }

    fun updateLengthDescription() {
        val show = showDescription.value ?: false
        showDescription.value = !show
    }

    private fun getBody(): HashMap<String, Any> {
        val body = HashMap<String, Any>()

        body["eventId"] = eventId
        body["name"] = "teste"
        body["email"] = "teste@teste.com"

        return body
    }
}
package com.pabloteigon.events.feature

import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.pabloteigon.events.R
import com.pabloteigon.events.data.exception.BadRequestException
import com.pabloteigon.events.data.exception.ConflictException
import com.pabloteigon.events.data.exception.UnauthorizedException
import com.pabloteigon.events.extension.snack
import com.pabloteigon.events.feature.viewmodel.BaseViewModel
import com.pabloteigon.events.feature.viewmodel.ViewState
import java.net.UnknownHostException

abstract class AbstractFragment : Fragment() {

    companion object {
        const val TAG = "AbstractFragment"
    }

    abstract val viewModel: BaseViewModel

    protected fun setupErrorState(parentSnackView: View) {
        viewModel.errorState.observe(this, Observer { state ->
            when (state) {
                is ViewState.Failed -> {
                    Log.e(TAG, "Failed " + state.throwable)
                    when (state.throwable) {
                        is UnauthorizedException -> {
                            parentSnackView.snack(message = getString(R.string.app_name), f = {})
                        }
                        is BadRequestException -> {
                            parentSnackView.snack(message = state.throwable.message!!, f = {})
                        }
                        is AccessDeniedException -> {
                            parentSnackView.snack(message = getString(R.string.app_name), f = {})
                        }
                        is ConflictException -> {
                            parentSnackView.snack(message = state.throwable.message!!, f = {})
                        }
                        is UnknownHostException -> {
                            parentSnackView.snack(
                                message = getString(R.string.internet_connection_error_msg),
                                f = {})
                        }
                        else -> {
                            parentSnackView.snack(message = state.throwable.message ?: "", f = {})
                        }
                    }
                }
            }
        })
    }

}
package com.pabloteigon.events.feature.events.details

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.navArgs
import com.pabloteigon.events.databinding.DetailsFragmentBinding
import com.pabloteigon.events.feature.AbstractFragment
import kotlinx.android.synthetic.main.details_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailsFragment : AbstractFragment() {

    override val viewModel: EventDetailsViewModel by viewModel()
    private lateinit var binding: DetailsFragmentBinding
    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.eventId = args.eventId

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        this.lifecycle.addObserver(viewModel)

        binding = DetailsFragmentBinding.inflate(inflater).apply {
            this.vm = viewModel
            lifecycleOwner = this@DetailsFragment
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ViewCompat.setTransitionName(binding.tvEventTitle, "name_${args.eventId}")
        ViewCompat.setTransitionName(binding.tvEventDescription, "duration_${args.eventId}")
        ViewCompat.setTransitionName(binding.ivEventImg, "thumbnail_${args.eventId}")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupErrorState(clDetailFragmentContainer)

        ivShare.setOnClickListener {
            shareEvent()
        }
    }

    private fun shareEvent() {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, viewModel.event.value?.getEventTextToShare())
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }
}

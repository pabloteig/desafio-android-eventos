package com.pabloteigon.events.di

import com.pabloteigon.events.feature.events.details.EventDetailsViewModel
import com.pabloteigon.events.feature.events.feed.EventFeedViewModel
import com.pabloteigon.events.feature.events.feed.EventsAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    factory { EventsAdapter() }

    viewModel {
        EventFeedViewModel(
            getListOfEventsUseCase = get(),
            uiScheduler = AndroidSchedulers.mainThread()
        )
    }

    viewModel {
        EventDetailsViewModel(
            getEventByIdUseCase = get(),
            checkInUseCase = get(),
            uiScheduler = AndroidSchedulers.mainThread()
        )
    }
}
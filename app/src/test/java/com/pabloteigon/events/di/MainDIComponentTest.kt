package com.pabloteigon.events.di

import com.pabloteigon.events.data.di.dataModules
import com.pabloteigon.events.domain.di.domainModule

/**
 * Main Koin DI component.
 * Helps to configure
 * 1) Mockwebserver
 * 2) Usecase
 * 3) Repository
 */
fun configureTestAppComponent(baseApi: String) = listOf(
    MockWebServerDIPTest,
    configureNetworkModuleForTest(baseApi),
    domainModule,
    dataModules
)

